const Sequelize = require("sequelize");
const sequelize = new Sequelize("DBWT19","root","root",{host:"localhost",dialect:"mysql",logging:false,port:3306});
const db={};

db.Sequelize = Sequelize;  
db.sequelize = sequelize;

db.Osoblje = sequelize.import(__dirname+'/static/osoblje.js');
db.Rezervacija = sequelize.import(__dirname+'/static/rezervacija1.js');
db.Sala = sequelize.import(__dirname+'/static/sala.js');
db.Termin = sequelize.import(__dirname+'/static/termin.js');

//veza 1-n osoblje-rezervacija
db.Osoblje.hasMany(db.Rezervacija,{foreignKey:'osoba'});
db.Rezervacija.belongsTo(db.Osoblje,{foreignKey:'osoba'});

//veza 1-1 rezervacija-termin
db.Termin.hasOne(db.Rezervacija,{foreignKey:{name:'termin',type:Sequelize.INTEGER,unique:true}});
db.Rezervacija.belongsTo(db.Termin,{foreignKey:{name:'termin',type:Sequelize.INTEGER,unique:true}});


//veza n-1 rezervacija-sala
db.Sala.hasMany(db.Rezervacija,{foreignKey:'sala'});
db.Rezervacija.belongsTo(db.Sala,{foreignKey:'sala'});

//veza 1-1 sala-osoblje
db.Osoblje.hasOne(db.Sala,{foreignKey:'zaduzenaOsoba'});
db.Sala.belongsTo(db.Osoblje,{foreignKey:'zaduzenaOsoba'});

module.exports = db;

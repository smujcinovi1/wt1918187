const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const app = express();
var path = require('path');
const db = require('./db.js');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('static'));
app.use(express.static(path.join(__dirname, 'static')));
app.use(express.static(__dirname));

app.get("/", function (req, res) {
    res.sendFile(__dirname + "/static/pocetna.html");
});

app.get("/", function (req, res) {
    res.sendFile(__dirname + "/zauzeca.json");
});

app.get("/", function (req, res) {
    res.sendFile(__dirname + "/static/rezervacija.html");
});

app.get("/", function (req, res) {
    res.sendFile(__dirname + "/static/pozivi.js");
});
app.get("/", function (req, res) {
    res.sendFile(__dirname + "/static/db.js");
});
app.get("/", function (req, res) {
    res.sendFile(__dirname + "/static/osoblje.html");
});
//treci zadatak

app.get("/prve", function (req, res) {
    res.write("{\"slika1\":\"/slika1.jpg\",\"slika2\":\"/slika2.jpg\",\"slika3\":\"/slika3.jpg\"}");
    res.send();
});

app.get("/druge", function (req, res) {
    res.write("{\"slika4\":\"/slika4.jpg\",\"slika5\":\"/slika5.jpg\",\"slika6\":\"/slika6.jpg\"}");
    res.send();
});
app.get("/trece", function (req, res) {
    res.write("{\"slika7\":\"/slika7.jpg\",\"slika8\":\"/slika8.jpg\",\"slika9\":\"/slika9.jpg\"}");
    res.send();
});
app.get("/cetvrte", function (req, res) {
    res.write("{\"slika10\":\"/slika10.jpg\"}");
    res.send();
});
//cetvrta spirala prvi zadatak
app.get("/osoblje", function (req, res) {
    console.log("u index osoblje");
    //u response treba upisati podatke dobijene iz baze
    //prvo trebam poslati upit kojim cu dobiti sve podatke iz tabele osoblje
    db.Osoblje.findAll({ attributes: ['id', 'ime', 'prezime', 'uloga'] }).then(function (osoblje) {
        res.json(osoblje);
        //res.send();
    });
    //console.log(osobljeBaza);

});

//cetvrta spirala drugi zadatak - ucitavanje podataka iz baze
app.get("/podaciIzBaze", function (req, res) {
    console.log("u podaci iz baze index");
    var per = [];
    var van = [];
    var el;
    db.Rezervacija.findAll({ include: [{ model: db.Sala }, { model: db.Termin }, { model: db.Osoblje }] }).then(function (rezervacije) {
        //u rezervacije se nalaze sve rezervacije iz baze

        console.log(rezervacije.length);
        console.log(rezervacije[0].Termin.redovni);
        console.log(rezervacije[1].Termin.redovni);
        for (var i = 0; i < rezervacije.length; i++) {
            var imePrezimeOsoba = rezervacije[i].Osoblje.ime + " " + rezervacije[i].Osoblje.prezime + " " + rezervacije[i].Osoblje.uloga;
            console.log(imePrezimeOsoba);
            if (!rezervacije[i].Termin.redovni) {
                el = { "datum": rezervacije[i].Termin.datum, "pocetak": rezervacije[i].Termin.pocetak, "kraj": rezervacije[i].Termin.kraj, "naziv": rezervacije[i].Sala.naziv, "predavac": imePrezimeOsoba };
                van.push(el);
            } else {
                el = { "dan": rezervacije[i].Termin.dan, "semestar": rezervacije[i].Termin.semestar, "pocetak": rezervacije[i].Termin.pocetak, "kraj": rezervacije[i].Termin.kraj, "naziv": rezervacije[i].Sala.naziv, "predavac": imePrezimeOsoba };
                per.push(el);
            }

        }
        console.log("periodicna el" + per.length);
        console.log("vanredna el " + van.length);
        res.write("{\"periodicna\":" + JSON.stringify(per) + ",\"vanredna\":" + JSON.stringify(van) + "}");
        res.send();
    });
});

//drugi zadatak
app.post("/zauzeca.json", function (req, res) {

    fs.readFile("zauzeca.json", "utf-8", function (err, data) {
        if (err) throw err;

        var podaciJson = JSON.parse(data);
        let periodicnaJSON = podaciJson.periodicna;
        let vanrednaJSON = podaciJson.vanredna;

        let proslijedjenaVrNiz = req.body;
        console.log("req.body");
        console.log(proslijedjenaVrNiz);
        //let proslijedjenaVrNiz = proslijedjenaVrString.split(",");

        let salaIzabrana = proslijedjenaVrNiz[5];
        let poc = proslijedjenaVrNiz[3];
        let end = proslijedjenaVrNiz[4];
        let periodicnaChecked = JSON.parse(proslijedjenaVrNiz[6]);
        let semester = proslijedjenaVrNiz[1];
        let mjesec = parseInt(proslijedjenaVrNiz[2]);
        let danNaKojiJeKliknuoUser = parseInt(proslijedjenaVrNiz[0]);
        let danUSedmici = parseInt(proslijedjenaVrNiz[7]);
        //ovdje sada kada je procitan json treba vrsiti provjeru nad ucitanim podacima 
        let mozeSeRezervisati;
        let mozeSeRezervisatiPer = true;
        let mozeSeRezervisatiVan = true;
        console.log("ispod body");

        // if (!periodicnaChecked) {
        //vanredna zauzeca provjeravamo
        //prije poziva ove fje pozvala sam ucitajZauzeca koja je azurirala ove nizove
        for (let i = 0; i < vanrednaJSON.length; i++) {
            console.log("u vanredna peltji");

            let element = vanrednaJSON[i];

            //ako je izabrana sala i pronadje se u listi vanrednih
            if (salaIzabrana.trim() != '' && element.naziv.localeCompare(salaIzabrana) === 0) {
                console.log("u sala");

                let danElement = parseInt(element.datum.substring(0, 2), 10);
                let mjesecElement = parseInt(element.datum.substring(3, 5), 10) - 1;
                //ako se i dani poklapaju
                if (danElement === danNaKojiJeKliknuoUser) {
                    //i mjesec
                    console.log("u dan");

                    if (mjesec === mjesecElement) {
                        //i vrijeme se poklapa
                        console.log("u mjesec");

                        if ((element.pocetak >= poc && element.pocetak < end) || (element.kraj > poc && element.kraj <= end) || (poc > element.pocetak && end < element.kraj)) {
                            //onda se ne moze rezervisati
                            console.log("u vrijeme");

                            mozeSeRezervisatiVan = false;
                            console.log(mozeSeRezervisatiVan);

                            //u svakom drugom slucaju moguce je rezervisati termin i ostaje true
                        }
                    }
                }
            }
        }
        //  } else {
        console.log("element dan");

        //provjeravamo periodicna zauzeca na taj dan
        for (let i = 0; i < periodicnaJSON.length; i++) {
            console.log("u petlji per");

            let element = periodicnaJSON[i];
            //ako je izabrana sala i pronadje se u listi vanrednih
            if (salaIzabrana.trim() != '' && element.naziv.localeCompare(salaIzabrana) === 0) {
                //ako je izabran dan u istom semestru u kojem vec postoji zauzece
                console.log("u sala");

                if (element.semestar === semester) {
                    console.log("u semestar");
                    console.log("element dan");
                    console.log(element.dan);
                    console.log("dan u sedmici");
                    console.log(danUSedmici);
                    //pa ako je izabran isti dan u sedmici
                    if (element.dan === danUSedmici) {

                        //ako se vremenski intervali poklapaju
                        if ((element.pocetak >= poc && element.pocetak < end) || (element.kraj > poc && element.kraj <= end) || (poc > element.pocetak && end < element.kraj)) {
                            console.log("u vrijeme");

                            mozeSeRezervisatiPer = false;
                            console.log(mozeSeRezervisatiPer);

                        }
                    }
                }
            }
            //   }

        }
        if (periodicnaChecked) {
            for (let i = 0; i < vanrednaJSON.length; i++) {
                console.log("u vanredna peltji");

                let element = vanrednaJSON[i];

                //ako je izabrana sala i pronadje se u listi vanrednih
                if (salaIzabrana.trim() != '' && element.naziv.localeCompare(salaIzabrana) === 0) {
                    console.log("u sala");

                    let danElement = parseInt(element.datum.substring(0, 2), 10);
                    let mjesecElement = parseInt(element.datum.substring(3, 5), 10);
                    if ((semester === "ljetni" && (mjesecElement >= 1 && mjesecElement <= 5)) || (semester === "zimski" && (mjesecElement === 9 || mjesecElement === 10 || mjesecElement === 11 || mjesecElement === 0))) {
                        //ako se i dani poklapaju
                        let dayString = 2019 + "-" + mjesecElement + "-" + danElement;

                        let danUSedmiciVanredni = (new Date(dayString)).getDay() - 1;
                        console.log("dan u sedmici vanredni ");
                        console.log(danUSedmiciVanredni);
                        if (danUSedmiciVanredni === danUSedmici) {
                            mozeSeRezervisatiPer = false;
                            break;
                        }
                    }
                }
            }
        }
        if (mozeSeRezervisatiPer == true && mozeSeRezervisatiVan == true) mozeSeRezervisati = true;
        else mozeSeRezervisati = false;
        if (mozeSeRezervisati) {
            //ide kod za kreiranje objekta i slanje post za upis u zauzeca.json
            let terminZauzeca;
            mjesec = mjesec + 1;
            if (!periodicnaChecked) {
                let godinaa = 2019;
                let dat, mj;
                if (danNaKojiJeKliknuoUser < 10) dat = "0" + danNaKojiJeKliknuoUser;
                else dat = danNaKojiJeKliknuoUser;
                if (mjesec < 10) mj = "0" + mjesec;
                else mj = mjesec;
                let datumZaJson = "" + dat + "." + mj + "." + godinaa;
                terminZauzeca = { datum: datumZaJson, pocetak: poc, kraj: end, naziv: salaIzabrana, predavac: "predavac neki" };
                vanrednaJSON.push(terminZauzeca);
            } else {
                terminZauzeca = { dan: danUSedmici, semestar: semester, pocetak: poc, kraj: end, naziv: salaIzabrana, predavac: "neki Predavac" };
                periodicnaJSON.push(terminZauzeca);
            }
        } else {
            //alert sa porukom da se ne moze rezervisati taj termin
            // alert("Nije moguće rezervisati salu " + salaIzabrana + " za navedeni datum " + danNaKojiJeKliknuoUser + "/" + mjesec + "/" + godina + " i termin od " + poc + " do " + end + "!");
        }

        let stringJson = "{ \"periodicna\":" + JSON.stringify(periodicnaJSON) + ", \"vanredna\":" + JSON.stringify(vanrednaJSON) + "}";
        mozeSeRezervisati = mozeSeRezervisati.toString();
        let posalji = stringJson + "&" + mozeSeRezervisati;

        fs.writeFile("zauzeca.json", stringJson, "utf-8", function (err) {
            if (err) throw err;
            //u response sam dodala jos jednu vrijednost boolean da li se moze dodati ili ne 
            res.json(posalji);
            console.log('Done!');

        });

    });

});

app.post("/upisiUBazu", function (req, res) {
    console.log(req.body);
    let terminZauzeca = req.body;
    let imePrezime = terminZauzeca.predavac.split(" ");
    let mozeSeRezervisatiServer = true;

    db.Osoblje.findAll().then(function (osoblje) {
        let osobeId = 0;
        for (let i = 0; i < osoblje.length; i++) {
            if ((osoblje[i].ime == imePrezime[0] && osoblje[i].prezime == imePrezime[1])) {
                osobeId = i + 1;
                break;
            }
        }
        db.Termin.findAll().then(function (termini) {
            var terminId = 0;
            terminId = termini.length + 1;



            db.Sala.findAll().then(function (sale) {
                let saleId = 0

                db.Rezervacija.findAll({ include: [{ model: db.Sala }, { model: db.Termin }, { model: db.Osoblje }] }).then(function (rezervacije) {
                    //u rezervacije imam sve rezervacije upravo ucitane iz baze podataka i sada ide provjera da li
                    //u njoj postoji rezervacija koju pokusavamo da upisemo

                    for (var i = 0; i < rezervacije.length; i++) {
                        if (rezervacije[i].Sala.naziv == terminZauzeca.naziv) {
                            if ((rezervacije[i].Termin.pocetak >= terminZauzeca.pocetak && rezervacije[i].Termin.pocetak < terminZauzeca.kraj) || (rezervacije[i].Termin.kraj > terminZauzeca.pocetak && rezervacije[i].Termin.kraj <= terminZauzeca.kraj) || (terminZauzeca.pocetak > rezervacije[i].Termin.pocetak && terminZauzeca.kraj < rezervacije[i].Termin.kraj)) {
                                if (rezervacije[i].Termin.redovni == false) {
                                    //vanredni termin
                                    if (rezervacije[i].Termin.datum == terminZauzeca.datum) {
                                        mozeSeRezervisatiServer = false;
                                        break;
                                    }
                                    //ako je termin zauzeca periodicna trebam provjeriti da li je vanredna 
                                    //rezervacija na taj dan u sedmici
                                    if (terminZauzeca.redovni == true) {
                                        //racunam koji je dan u sedmici datum vanrednog zauzeca u rezervacije
                                        let danUSedmiciVanredni = (new Date(rezervacije[i].Termin.datum)).getDay() - 1;
                                        if (danUSedmiciVanredni < 0) danUSedmiciVanredni = 6;
                                        if(danUSedmiciVanredni === terminZauzeca.dan){
                                            mozeSeRezervisatiServer = false;
                                            break;
                                        }
                                    }
                                } else {
                                    //periodicni termin
                                    if (rezervacije[i].Termin.dan == terminZauzeca.dan & rezervacije[i].Termin.semestar == terminZauzeca.semestar) {
                                        mozeSeRezervisatiServer = false;
                                        break;
                                    }
                                    //ako je termin zauzeca vanredni 
                                    //trebam provjeriti da li postoji periodicna rezervacija na taj dan
                                    if (terminZauzeca.redovni == false) {
                                        //racunam koji je dan u sedmici datum vanrednog zauzeca u rezervacije
                                        let danUSedmiciVanredni = (new Date(terminZauzeca.datum)).getDay() - 1;
                                        if (danUSedmiciVanredni < 0) danUSedmiciVanredni = 6;
                                        if(danUSedmiciVanredni === rezervacije[i].Termin.dan){
                                            mozeSeRezervisatiServer = false;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (mozeSeRezervisatiServer) {
                        db.Sala.create({ naziv: terminZauzeca.naziv, zaduzenaOsoba: osobeId });
                        saleId = sale.length + 1;

                        db.Termin.create({ redovni: terminZauzeca.redovni, dan: terminZauzeca.dan, datum: terminZauzeca.datum, semestar: terminZauzeca.semestar, pocetak: terminZauzeca.pocetak, kraj: terminZauzeca.kraj }).then(function (k) {
                            db.Rezervacija.create({ osoba: osobeId, termin: terminId, sala: saleId });
                        });
                        
                    }
                    
                });

            });
        });
    });
    setTimeout(function(){
        res.write(JSON.stringify(mozeSeRezervisatiServer));
        res.send();
    },1800);

   /* res.write(JSON.stringify(mozeSeRezervisatiServer));
    res.send();*/

});

app.get("/osobeISale", function (req, res) {
    console.log("u osobe i sale index");

    var jsonOsobeISale = [];

    db.Rezervacija.findAll({ include: [{ model: db.Sala }, { model: db.Osoblje }, { model: db.Termin }] }).then(function (rezervacije) {
        let trenutniDatum = new Date();
        let sati, minute, sekunde;
        sati = trenutniDatum.getHours();
        if (sati < 10) sati = "0" + sati;
        minute = trenutniDatum.getMinutes();
        if (minute < 10) minute = "0" + minute;
        sekunde = trenutniDatum.getSeconds();
        if (sekunde < 10) sekunde = "0" + sekunde;
        let trenutnoVrijeme = "" + sati + ":" + minute + ":" + sekunde;
        console.log("trenutno vrijeme: " + trenutnoVrijeme);


        let danasnjiDatum = trenutniDatum.getDate();
        if (danasnjiDatum < 10) danasnjiDatum = "0" + danasnjiDatum;
        let trenutniMjesec = (trenutniDatum.getMonth() + 1).toString();
        if (parseInt(trenutniMjesec) < 10) trenutniMjesec = "0" + trenutniMjesec;
        danasnjiDatum = "" + danasnjiDatum + "." + trenutniMjesec + "." + 2019;
        let danasnjiDatum5 = trenutniDatum.getDate();
        if (danasnjiDatum5 < 10) danasnjiDatum5 = "0" + danasnjiDatum5;
        let danasnjiDatum2019String = "2019-" + trenutniMjesec + "-" + danasnjiDatum5;
        console.log(danasnjiDatum2019String);
        let danasnjiDatum2019 = new Date(danasnjiDatum2019String);
        let danasnjiDan = danasnjiDatum2019.getDay() - 1;
        if (danasnjiDan < 0) danasnjiDan = 6;
        let semestar;
        if (parseInt(trenutniMjesec) === 1 || parseInt(trenutniMjesec) === 10 || parseInt(trenutniMjesec) === 11 || parseInt(trenutniMjesec) === 12) semestar = "zimski";
        else if (parseInt(trenutniMjesec) >= 2 && parseInt(trenutniMjesec) <= 6) semestar = "ljetni";
        console.log("trenutni datum: " + danasnjiDatum);
        console.log("danasnji dan " + danasnjiDan);

        //ako neka osoba nije nikako napravila rezervaciju za nju cemo ispisati da je u kancelariji
        /*   db.Osoblje.findAll().then(function(osobe){
               console.log("osoblje length: " + osobe.length);
               let rezervisaoSalu = false;
               for(let j=0;j<osobe.length;j++){
                   console.log("prva for petlja, osoba[i] " + osobe[j].id);
                   for(let k=0;k<rezervacije.length;k++){
                       if(osobe[j].id === rezervacije[k].Osoblje.id){
                           rezervisaoSalu = true;
                           break;
                       }
                   }
                   if(!rezervisaoSalu){
                       console.log("ne bi trebao uci u ovaj uslov");
                       let imeOsoba = osobe[j].ime + " " + osobe[j].prezime;
                       jsonOsobeISale.push({osoba:imeOsoba,sala:"u kancelariji"});
                   }
               }
           });*/


        for (let i = 0; i < rezervacije.length; i++) {
            let upisi = false;
            let indexRezervacije;
            let imeOsobe = rezervacije[i].Osoblje.ime + " " + rezervacije[i].Osoblje.prezime;
            console.log("druga for petlja rezervacije.length " + rezervacije.length);
            //ako se termin rezervacije poklapa sa sadasnjim, tj vrijeme prvo cemo provjeriti pa onda dan i datum
            console.log("rez pocetak: " + rezervacije[i].Termin.pocetak + " rez kraj " + rezervacije[i].Termin.kraj);
            if (trenutnoVrijeme >= rezervacije[i].Termin.pocetak && trenutnoVrijeme <= rezervacije[i].Termin.kraj) {
                console.log("poklopilo se vrijeme");
                //sad razdvajam na periodicne i vanredne 
                if (!rezervacije[i].Termin.redovni) {
                    //ako su vanredne
                    console.log("vanredna");
                    console.log(rezervacije[i].Termin.datum);
                    if (rezervacije[i].Termin.datum == danasnjiDatum) {
                        console.log("poklopili se datumi");
                        upisi = true;
                        indexRezervacije = i;
                    }
                } else {
                    console.log("periodicna");
                    //ako su periodicne
                    console.log("rez dan: " + rezervacije[i].Termin.dan + "danasnji dan: " + danasnjiDan);
                    console.log("rez semestar: " + rezervacije[i].Termin.semestar + "danasnji semestar: " + semestar);

                    if (rezervacije[i].Termin.dan === danasnjiDan && rezervacije[i].Termin.semestar == semestar) {
                        console.log("poklopio se dan i semestar");
                        upisi = true;
                        indexRezervacije = i;
                    }
                }
            }
            if (upisi) {
                jsonOsobeISale.push({ osoba: imeOsobe, sala: rezervacije[indexRezervacije].Sala.naziv });
            }
        }

        console.log("velicina json nakon zavrsenog upisivanja onih koji su fakat zauzeli sale " + jsonOsobeISale.length);

        db.Osoblje.findAll().then(function (osobe) {
            console.log("osoblje length: " + osobe.length);
            for (let j = 0; j < osobe.length; j++) {
                let upisanUJson = false;
                console.log(jsonOsobeISale.length);
                console.log("prva for petlja, osoba[i] " + osobe[j].id);
                for (let k = 0; k < jsonOsobeISale.length; k++) {
                    let jsonOsobaImePrezime = jsonOsobeISale[k].osoba.split(" ");
                    if (osobe[j].ime == jsonOsobaImePrezime[0] && osobe[j].prezime == jsonOsobaImePrezime[1]) {
                        upisanUJson = true;
                        break;
                    }
                }
                if (!upisanUJson) {
                    console.log("u !uspisanUJson");
                    let imeOsoba = osobe[j].ime + " " + osobe[j].prezime;
                    jsonOsobeISale.push({ osoba: imeOsoba, sala: "u kancelariji" });
                }
            }
            console.log("na kraju je vel jsona: " + jsonOsobeISale.length);

            let string = "{\"lista\":" + JSON.stringify(jsonOsobeISale) + "}";
            res.write(string);
            res.send();
        });


    });

});

//kod za potrebe testova na cetvrtoj spirali
app.get("/sale", function (req, res) {
    console.log("u index sale");
    //u response treba upisati podatke dobijene iz baze
    //prvo trebam poslati upit kojim cu dobiti sve podatke iz tabele osoblje
    db.Sala.findAll({ attributes: ['id', 'naziv', 'zaduzenaOsoba'] }).then(function (sale) {
        res.json(sale);
        //res.send();
    });
    //console.log(osobljeBaza);

});
module.exports = app;
app.listen(8080);

let trenutniDatum = new Date();
let mjesecTrenutni = trenutniDatum.getMonth();
let godina = 2019;
let mjeseciUGodini = ["Januar", "Februar", "Mart", "April", "Maj", "Juni", "Juli", "August", "Septembar", "Oktobar", "Novembar", "Decembar"];
let danNaKojiJeKliknuoUser;
let mjesecUKojemJeUserKliknuo;
let danUSedmici;

let Kalendar = (function () {
    //ovdje idu privatni atributi   
    //zakomentarisala sam ih jer se sada nalaze u pozivi.js
    /*  let periodicnaGlavna=[];
      let vanrednaGlavna=[]; 
      */

    function obojiZauzecaImpl(kalendarRef, mjesec, sala, pocetak, kraj) {
        console.log("periodicna glavna u kalendar js");
        console.log(periodicnaGlavna.length);
        for (let i = 0; i < periodicnaGlavna.length; i++) {
            let element = periodicnaGlavna[i];
            //ako je izabrana sala i pronadje se u listi periodicna
            if (sala.trim() != '' && element.naziv.localeCompare(sala) === 0) {
                //ako su popunjena polja za pocetak i kraj
                if (pocetak.trim != '' && kraj.trim() != '') {
                    //ide ogromni uslov za intervale vremena
                    console.log("uneseno:" + pocetak);
                    console.log("zapisano:" + element.pocetak);
                    if ((element.pocetak >= pocetak && element.pocetak < kraj) || (element.kraj > pocetak && element.kraj <= kraj) || (pocetak > element.pocetak && kraj < element.kraj)) {
                        var sem = "zimski";
                        var elementSemestar = element.semestar;
                        //poredi se koji je semestar da znamo koje cemo mjesece obojiti
                        //ako je zimski onda se boje mjeseci od oktobra do januara
                        //ljetni pocinje u februaru a zavrsava u junu
                        if (elementSemestar.localeCompare(sem) === 0 && (mjesec === 9 || mjesec === 10 || mjesec === 11 || mjesec === 0)) {
                            let k = element.dan;
                            console.log(k);
                            for (let j = k; j < document.querySelectorAll("body>div>div>table>tbody>tr>td").length; j++) {
                                if (document.querySelectorAll("body>div>div>table>tbody>tr>td")[j].childElementCount != 0) {
                                    console.log(j);
                                    if (j === k || j === k + 7) {
                                        document.querySelectorAll("body>div>div>table>tbody>tr>td")[j].querySelectorAll("tr")[1].querySelector("th").setAttribute("class", "zauzeta");
                                        k += 7;
                                    }
                                }
                            }

                        } else if (element.semestar.localeCompare("ljetni") === 0 && (mjesec >= 1 && mjesec <= 5)) {
                            let k = element.dan;
                            for (let j = k; j < document.querySelectorAll("body>div>div>table>tbody>tr>td").length; j++) {
                                if (document.querySelectorAll("body>div>div>table>tbody>tr>td")[j].childElementCount != 0) {
                                    if (j === k || j === k + 7) {
                                        document.querySelectorAll("body>div>div>table>tbody>tr>td")[j].querySelectorAll("tr")[1].querySelector("th").setAttribute("class", "zauzeta");
                                        k += 7;
                                    }
                                }
                            }

                        }
                    }
                }
            }
        }

        //sada pretrazujemo vanredna zauzeca
        for (let i = 0; i < vanrednaGlavna.length; i++) {
            let element = vanrednaGlavna[i];
            //ako je izabrana sala i pronadje se u listi vanrednih
            if (sala.trim() != '' && element.naziv.localeCompare(sala) === 0) {
                //uslov za poklapanje vremenskih intervala
                if ((element.pocetak >= pocetak && element.pocetak < kraj) || (element.kraj > pocetak && element.kraj <= kraj) || (pocetak > element.pocetak && kraj < element.kraj)) {

                    let danElement = parseInt(element.datum.substring(0, 2), 10);
                    let mjesecElement = parseInt(element.datum.substring(3, 5), 10) - 1;
                    let brojac = 1;
                    let prviDanUMjesecu = (new Date(godina, mjesecElement, 1)).getDay();
                    if (prviDanUMjesecu != 0) {
                        prviDanUMjesecu = prviDanUMjesecu - 1;
                    }
                    else {
                        prviDanUMjesecu = 6;
                    }
                    if (mjesec === mjesecElement) {
                        for (let j = prviDanUMjesecu; j < document.querySelectorAll("body>div>div>table>tbody>tr>td").length; j++) {
                            if (brojac === danElement) {
                                document.querySelectorAll("body>div>div>table>tbody>tr>td")[j].querySelectorAll("tr")[1].querySelector("th").setAttribute("class", "zauzeta");
                                break;
                            }
                            else brojac++;
                        }
                    }
                }
            }
        }
    }
    function ucitajPodatkeImpl(periodicna, vanredna) {
        //zakomentarisana je fja zato sto se sada podaci ucitavaju u pozivi.js
        //validacija podataka iz poslanih listi
        //dan mora biti u intervalu od 0 do 6
        //vrijeme u intervalu od 
        /* periodicnaGlavna=[];
         vanrednaGlavna=[];
        console.log(periodicna.length);
        for(let i=0; i<periodicna.length;i++){
            let element = periodicna[i];
            if(element.dan >=0 && element.dan <=6 && (element.semestar==="zimski" || element.semestar==="ljetni")){
                periodicnaGlavna.push(element);
            }
        }
        console.log(periodicnaGlavna.length);

        for(let i=0; i<vanredna.length;i++){
            let element = vanredna[i];
            if(parseInt(element.datum.substring(0,2),10) <= 31 && parseInt(element.datum.substring(0,2),10) >= 1 && parseInt(element.datum.substring(3,5),10)>=0 && parseInt(element.datum.substring(3,5),10)<=11){
                vanrednaGlavna.push(element);
            }
        }
        Kalendar.iscrtajKalendar(document.getElementById("calendar-body"),mjesecTrenutni);
        console.log(vanrednaGlavna.length);
        */
    }

    function iscrtajKalendarImpl(kalendarRef, mjesec) {
        //implementacija ide ovdje   
        let godina = 2019;

        let prviDanUMjesecu = (new Date(godina, mjesec, 1)).getDay();
        if (prviDanUMjesecu != 0) {
            prviDanUMjesecu = prviDanUMjesecu - 1;
        }
        else {
            prviDanUMjesecu = 6;
        }

        let brojDanaUMjesecu = 32 - new Date(godina, mjesec, 32).getDate();

        let nazivMjesecaTrenutnog = document.getElementById("nazivMjeseca");
        // let kalendar = document.getElementsByClassName("kalendarDiv"); // div u kojem mi se nalazi kalendar
        kalendarRef.innerHTML = ""; //brisanje svih prijasnjih podataka

        nazivMjesecaTrenutnog.innerHTML = mjeseciUGodini[mjesec];

        //kreiranje celija
        let dann = 1;
        for (let i = 0; i < 6; i++) {
            // kreiranje redova
            let row = document.createElement("tr");
            //kreiranje celija i popunjavanje sa podacima
            for (let j = 0; j < 7; j++) {
                if (i === 0 && j < prviDanUMjesecu) {
                    let celija = document.createElement("td");
                    let cellText = document.createTextNode("");
                    celija.appendChild(cellText);
                    row.appendChild(celija);
                }
                else if (dann > brojDanaUMjesecu) {
                    break;
                }

                else {
                    let cell = document.createElement("td");
                    cell.setAttribute("class", "tabelaCelija");
                    let rowDat = document.createElement("tr");
                    let celijaDat = document.createElement("th");
                    celijaDat.setAttribute("class", "datum");
                    let cellDat = document.createTextNode(dann);
                    celijaDat.appendChild(cellDat);
                    rowDat.appendChild(celijaDat);
                    cell.appendChild(rowDat);
                    let rowBoja = document.createElement("tr");
                    let celijaBoja = document.createElement("th");
                    celijaBoja.setAttribute("class", "slobodna");
                    let cellBoja = document.createTextNode("");
                    celijaBoja.appendChild(cellBoja);
                    rowBoja.appendChild(celijaBoja);
                    cell.appendChild(rowBoja);
                    row.appendChild(cell);
                    dann++;
                }
            }
            kalendarRef.appendChild(row); // dodavanje svakog reda u kalendar
        }
        //ovaj listener sam dodala da osluskuje kada se klikne na neki od datuma u kalendaru
        for (let i = 0; i < document.getElementById("calendar-body").getElementsByTagName("td").length; i++) {
            if (document.getElementById("calendar-body").getElementsByTagName("td")[i].childElementCount != 0) {
                document.getElementById("calendar-body").getElementsByTagName("td")[i].addEventListener("click", function (event) {
                    var x = document.getElementById("calendar-body").getElementsByTagName("td")[i];
                    console.log(x);
                    var datKojiJeIzabran = document.getElementById("calendar-body").getElementsByTagName("td")[i].querySelector(".datum").innerText;
                    console.log(datKojiJeIzabran);
                    //sljedeci if uslov provjerava da li je kliknuto na broj tj datum ili negdje onako random na stranici
                    if (!isNaN(datKojiJeIzabran)) {
                        danNaKojiJeKliknuoUser = parseInt(x.innerText, 10);
                        //treba nam i mjesec u kojem je kliknuto
                        mjesecUKojemJeUserKliknuo = document.getElementById("nazivMjeseca").textContent;
                        let brojMjeseca;
                        for (let i = 0; i < 12; i++) {
                            if (mjesecUKojemJeUserKliknuo.localeCompare(mjeseciUGodini[i]) === 0) {
                                brojMjeseca = i;
                                break;
                            }
                        }
                        //treba odrediti koji je dan u sedmici danNaKojiJeUserKliknuo 
                        let nadjeno = false;
                        var tabela = document.getElementsByClassName("kalendarTabela")[0];
                        //u redovi se nalaze svi redovi iz tabele
                        var redovi = tabela.rows;
                        //vanjska petlja sluzi da prolazimo kroz svaki red posebno
                        for (let i = 1; i < redovi.length; i++) {
                            //iz svakog reda izdvajamo kolone
                            var red = redovi[i].cells;
                            console.log("red");
                            console.log(red.length);
                            //unutrasnjom petljom prolazimo kroz svaku celiju tabele
                            for (let j = 0; j < red.length; j++) {
                                if (red[j].querySelector("tr") != null && red[j].querySelector("tr").innerText === danNaKojiJeKliknuoUser.toString()) {
                                    danUSedmici = j;
                                    nadjeno = true;
                                    break;
                                }
                            }
                            if (nadjeno) break;
                        }
                        console.log("dan u sedmici");
                        console.log(danUSedmici);

                        //sljedeci if uslov provjerava da li je izabrano periodicno ili ne i u skladu sa tim 
                        //ce ispisati poruku u alertu
                        //i kasnije cemo na osnovu toga znati u koju listu se dodaju zauzeca
                        if (!periodicnaChecked) {
                            //provjeravamo da li je datum u proslosti jer se on ne moze rezervisati za vanredna zauzeca
                            //  if (danNaKojiJeKliknuoUser > trenutniDatum.getDay() && brojMjeseca >= trenutniDatum.getMonth()) {
                            //confirm("Zelite li rezervisati ovaj termin " + danNaKojiJeKliknuoUser + ". " + mjesecUKojemJeUserKliknuo + " " + godina + "?");
                            //ovdje sada treba ici dio koda koji se izvrsava klikom na OK u alertu
                            if (confirm("Zelite li rezervisati ovaj termin " + danNaKojiJeKliknuoUser + ". " + mjesecUKojemJeUserKliknuo + " " + godina + "?")) {
                                //ovaj if uslov je ako smo kliknuli na OK
                                //sada provjeravamo da li postoji zauzece na serveru
                                //Pozivi.ucitajPodatkeIzBaze();
                                //nakon toga bi trebalo pozvati fju dodaj u json file koja bi se implementirala u pozivi
                                Pozivi.upisiUBazu();
                            } else {
                                //ako se klikne na cancel neka se ponovo iscrta kalendar tj vrati na prvobitno
                                Kalendar.iscrtajKalendar(document.getElementById("calendar-body"), mjesecTrenutni);
                            }
                            //  } else {
                            /*
                            alert("Ne mozete vanredno rezervisati datum u proslosti!");
                            Kalendar.iscrtajKalendar(document.getElementById("calendar-body"), mjesecTrenutni);
                            */
                            //  }
                        } else {

                            //za periodicna se moze otici npr u neki mjesec u ljetnom ili zimskom i tad kliknemo da bi nam rezervisalo npr za slj god
                            if ((brojMjeseca === 9 || brojMjeseca === 10 || brojMjeseca === 11 || brojMjeseca === 0)) {
                                //confirm("Zelite li rezervisati ovaj termin " + danNaKojiJeKliknuoUser + ". dan u mjesecu u zimskom semestru?");
                                //ovdje ide dio koda koji se izvrsava klikom na ok u alertu
                                if (confirm("Zelite li rezervisati ovaj termin " + danNaKojiJeKliknuoUser + ". dan u mjesecu u zimskom semestru?")) {
                                    //Pozivi.ucitajPodatkeIzBaze();
                                    Pozivi.upisiUBazu();
                                } else {
                                    //ako se klikne na cancel neka se ponovo iscrta kalendar tj vrati na prvobitno
                                    Kalendar.iscrtajKalendar(document.getElementById("calendar-body"), mjesecTrenutni);
                                }
                            } else if (brojMjeseca >= 1 && brojMjeseca <= 5) {
                                //confirm("Zelite li rezervisati ovaj termin " + danNaKojiJeKliknuoUser + ". dan u mjesecu u ljetnom semestru?");
                                //ovdje ide dio koda koji se izvrsava kada se klikne ok u alertu
                                if (confirm("Zelite li rezervisati ovaj termin " + danNaKojiJeKliknuoUser + ". dan u mjesecu u ljetnom semestru?")) {
                                    // Pozivi.ucitajPodatkeIzBaze();
                                    Pozivi.upisiUBazu();
                                } else {
                                    //ako se klikne na cancel neka se ponovo iscrta kalendar tj vrati na prvobitno
                                    Kalendar.iscrtajKalendar(document.getElementById("calendar-body"), mjesecTrenutni);
                                }
                            }
                            else {
                                alert("Ovaj mjesec ne pripada ni ljetnom ni zimskom semestru!");
                            }
                            promjenaForme();
                        }

                    }
                });
            }
        }
    }
    return {
        obojiZauzeca: obojiZauzecaImpl,
        //ucitajPodatke: ucitajPodatkeImpl,        
        iscrtajKalendar: iscrtajKalendarImpl
    }
}());
//ucita podatke u privatne atribute koji sad predstavljaju kao neku bazu
//Kalendar.ucitajPodatke([{dan:3,semestar:"ljetni",pocetak:"13:00",kraj:"16:00",naziv:"0-02",predavac:"prof1"},
 //                       {dan:2,semestar:"zimski",pocetak:"12:30",kraj:"14:30",naziv:"0-09",predavac:"prof2"},
 //                       {dan:1,semestar:"ljetni",pocetak:"15:00",kraj:"17:00",naziv:"0-03",predavac:"prof3"},
  //                      {dan:6,semestar:"zimski",pocetak:"11:00",kraj:"13:00",naziv:"MA",predavac:"prof4"}],[{datum:"23.10.2019",pocetak:"13:00",kraj:"19:00",naziv:"0-02",predavac:"prof2"}]);

//zatim se iscrtava kalendar
/*
Kalendar.iscrtajKalendar(document.getElementById("calendar-body"),mjesecTrenutni);
//funkcija koja poziva obojiZauzeca svaki put kada se promijeni nesto na formi iznad kalendara
function promjenaForme(){
    let mjesecPrikazani = document.getElementById("nazivMjeseca").textContent;
    let brojTrenutnogMjeseca;
    for(let i=0;i<12;i++){
        if(mjesecPrikazani.localeCompare(mjeseciUGodini[i])===0){
            brojTrenutnogMjeseca = i;
            break;
        }
    }
    console.log(brojTrenutnogMjeseca);
    let salaIzabrana = document.getElementById("saleIzaberi").value;
    let poc = document.getElementById("pocetak").value;
    let end = document.getElementById("kraj").value;
    Kalendar.iscrtajKalendar(document.getElementById("calendar-body"),brojTrenutnogMjeseca);
    Kalendar.obojiZauzeca(document.getElementById("calendar-body"), brojTrenutnogMjeseca, salaIzabrana, poc,end);
}

function sljedeci() {
        godina = (new Date()).getFullYear();
        let mjesecPrikazani = document.getElementById("nazivMjeseca").textContent;
        let brojTrenutnogMjeseca;
        for(let i=0;i<12;i++){
            if(mjesecPrikazani.localeCompare(mjeseciUGodini[i])===0){
                brojTrenutnogMjeseca = i;
                break;
            }
        }
        //koristimo brojTrenutnogMjeseca jer nam treba sljedbenik od trenutno prikazanog mjeseca
        if(brojTrenutnogMjeseca+1 <= 11){
            brojTrenutnogMjeseca++;
            Kalendar.iscrtajKalendar(document.getElementById("calendar-body"), brojTrenutnogMjeseca);

            let salaIzabrana = document.getElementById("saleIzaberi").value;
            let poc = document.getElementById("pocetak").value;
            let end = document.getElementById("kraj").value;
            Kalendar.obojiZauzeca(document.getElementById("calendar-body"), brojTrenutnogMjeseca, salaIzabrana, poc,end);
        }else{
            document.getElementById("dva").disabled = true;
        }
    }

    function prethodni() {
        godina = (new Date()).getFullYear();
        let mjesecPrikazani = document.getElementById("nazivMjeseca").textContent;
        let brojTrenutnogMjeseca;
        for(let i=0;i<12;i++){
            if(mjesecPrikazani.localeCompare(mjeseciUGodini[i])===0){
                brojTrenutnogMjeseca = i;
                break;
            }
        }
        //koristimo brojTrenutnogMjeseca jer nam treba prethodnik od trenutno prikazanog mjeseca
        if(brojTrenutnogMjeseca-1 >= 0){
            brojTrenutnogMjeseca--;
            Kalendar.iscrtajKalendar(document.getElementById("calendar-body"), brojTrenutnogMjeseca);
            let salaIzabrana = document.getElementById("saleIzaberi").value;
            let poc = document.getElementById("pocetak").value;
            let end = document.getElementById("kraj").value;
            Kalendar.obojiZauzeca(document.getElementById("calendar-body"), brojTrenutnogMjeseca, salaIzabrana, poc,end);
        }else{
            document.getElementById("jedan").disabled = true;
        }
    }*/
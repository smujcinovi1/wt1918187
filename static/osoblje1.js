let i = 0;
Pozivi.ucitajOsobeISale();
setInterval(function () {
    Pozivi.ucitajOsobeISale();
}, 30000);
/*<div class="tabela">
        <table id="tabelaOsoba">
            <thead id="head">
                <tr id="prvi">
                    <th class="naslovKolone">Predavac</th>
                    <th class="naslovKolone">Sala</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th class="naslovKolone">Predavac</th>
                    <th class="naslovKolone">Sala</th>
                </tr>
            </tbody>
        </div>*/
function napraviTabelu(responseJson) {
    //responseJson mi je lista objekata sa imenima osoba i salama koje su zauzeli
    var sadrzaj = document.getElementsByClassName("sadrzaj")[0];
    sadrzaj.innerHTML = "";

    var tabelaDiv = document.createElement("div");

    var tbl = document.createElement("table"); // Get the table
    tbl.setAttribute("id","tabelaOsoba");
    tbl.style.margin = "auto";

    var thead = document.createElement("thead");
    thead.setAttribute("id", "head");

    var redPrvi = document.createElement("tr");
    redPrvi.setAttribute("id", "prvi");

    var kolona1 = document.createElement("th");
    kolona1.setAttribute("class","naslovKolone");
    var text = document.createTextNode("Predavac");
    kolona1.appendChild(text);
    redPrvi.appendChild(kolona1);

    var kolona2 = document.createElement("th");
    kolona2.setAttribute("class","naslovKolone");
    var text = document.createTextNode("Sala");
    kolona2.appendChild(text);
    redPrvi.appendChild(kolona2);

    thead.appendChild(redPrvi);
    tbl.appendChild(thead);

    var tbody = document.createElement("tbody");
    tbody.setAttribute("id", "table-body");
   
        
    i++;
    if (responseJson.length) {
        for (let i = 0; i < responseJson.length; i++) {
            let row = document.createElement("tr");
            //kreiranje celija i popunjavanje sa podacima
            for (let j = 0; j < 2; j++) {
               
                    
                    let osoba = document.createElement("td");
                    osoba.setAttribute("class", "osobaIme");
                    let cellTextOsoba;
                    if(j === 0){
                    cellTextOsoba= document.createTextNode(responseJson[i].osoba);
                    }else{
                        cellTextOsoba= document.createTextNode(responseJson[i].sala);
                    }
                    osoba.appendChild(cellTextOsoba);
                    row.appendChild(osoba);
                
                   /* 
                    let sala = document.createElement("td");
                    sala.setAttribute("class", "salaNaziv");
                    let cellTextSala = document.createTextNode(responseJson[i].sala);
                    osoba.appendChild(cellTextSala);
                    row.appendChild(sala);*/
                
            }
            tbody.appendChild(row);
        }

       tbl.appendChild(tbody);
        tabelaDiv.appendChild(tbl);
        sadrzaj.appendChild(tabelaDiv);
        
    }
}
//ovo je Pozivi modul za ajax pozive
//prvo trebam poslati zahtjev da mi se u liste zauzeca ucitaju podaci sa servera
let periodicnaGlavna = [];
let vanrednaGlavna = [];
let kes = [];
let Pozivi = (function () {

    function ucitajZauzecaAjaxImpl() {
        var ajax = new XMLHttpRequest();
        console.log("u pozivi.js je");
        ajax.onreadystatechange = function () {// Anonimna funkcija
            if (ajax.readyState == 4 && ajax.status == 200) {
                let data = JSON.parse(ajax.responseText);

                let periodicnaJSON = data.periodicna;
                let vanrednaJSON = data.vanredna;
                //sljedece dvije linije su tu da se ne bi duplo upisivalo u ove liste
                periodicnaGlavna = [];
                vanrednaGlavna = [];
                console.log("periodicnaJSON ima elemenata:");
                console.log(vanrednaJSON.length);
                $.each(periodicnaJSON, function () {
                    let elJsonP = { dan: this.dan, semestar: this.semestar, pocetak: this.pocetak, kraj: this.kraj, naziv: this.naziv, predavac: this.predavac };
                    periodicnaGlavna.push(elJsonP);
                });
                $.each(vanrednaJSON, function () {
                    let elJsonV = { datum: this.datum, pocetak: this.pocetak, kraj: this.kraj, naziv: this.naziv, predavac: this.predavac };
                    vanrednaGlavna.push(elJsonV);
                });

            }
        }
        ajax.open("GET", "http://localhost:8080/zauzeca.json", false);
        ajax.send();
    }

    function ucitajPodatkeIzBazeImpl() {
        console.log("u pozivi ucitaj iz baze");
        var ajax = new XMLHttpRequest();
        console.log("u pozivi.js je");
        ajax.onreadystatechange = function () {// Anonimna funkcija
            if (ajax.readyState == 4 && ajax.status == 200) {
                let data = JSON.parse(ajax.response);
                console.log("data" + data);
                let periodicnaJSON = data.periodicna;
                let vanrednaJSON = data.vanredna;
                //sljedece dvije linije su tu da se ne bi duplo upisivalo u ove liste
                periodicnaGlavna = [];
                vanrednaGlavna = [];
                console.log("periodicnaJSON ima elemenata:");
                console.log(vanrednaJSON);
                $.each(periodicnaJSON, function () {
                    let elJsonP = { dan: this.dan, semestar: this.semestar, pocetak: this.pocetak, kraj: this.kraj, naziv: this.naziv, predavac: this.predavac };
                    periodicnaGlavna.push(elJsonP);
                });
                $.each(vanrednaJSON, function () {
                    let elJsonV = { datum: this.datum, pocetak: this.pocetak, kraj: this.kraj, naziv: this.naziv, predavac: this.predavac };
                    vanrednaGlavna.push(elJsonV);
                });

            }
        }
        ajax.open("GET", "podaciIzBaze", false);
        ajax.send();
    }
    function upisiUZauzecaJsonImpl() {
        var ajax2 = new XMLHttpRequest();
        let salaIzabrana = document.getElementById("saleIzaberi").value;
        let poc = document.getElementById("pocetak").value;
        let end = document.getElementById("kraj").value;
        periodicnaChecked = document.getElementById("check").checked;
        let semester;
        let mjesec;
        for (let i = 0; i < 12; i++) {
            if (mjesecUKojemJeUserKliknuo.localeCompare(mjeseciUGodini[i]) === 0) {
                mjesec = i;
                break;
            }
        }
        if (mjesec === 9 || mjesec === 10 || mjesec === 11 || mjesec === 0) semester = "zimski";
        else if (mjesec >= 1 && mjesec <= 5) semester = "ljetni";
        //u jedan niz stavljamo sve ove vrijednosti koje nam trebaju da napravimo objekat te ih saljemo 
        //kao req u index.js

        let proslijedjenaVrijednost = [];

        proslijedjenaVrijednost.push(danNaKojiJeKliknuoUser.toString());
        proslijedjenaVrijednost.push(semester);
        proslijedjenaVrijednost.push(mjesec.toString());
        proslijedjenaVrijednost.push(poc);
        proslijedjenaVrijednost.push(end);
        proslijedjenaVrijednost.push(salaIzabrana);
        proslijedjenaVrijednost.push(periodicnaChecked.toString());
        proslijedjenaVrijednost.push(danUSedmici.toString());

        console.log(JSON.stringify(proslijedjenaVrijednost));
        //ovdje treba samo open i send
        let proslijedjenaVrijednostString = "{\"vrijednosti\":" + JSON.stringify(proslijedjenaVrijednost) + "}";
        ajax2.open("POST", "http://localhost:8080/zauzeca.json", true);
        ajax2.setRequestHeader("Content-Type", "application/json");
        ajax2.send(JSON.stringify(proslijedjenaVrijednost));
        ajax2.onreadystatechange = function () {
            if (ajax2.readyState == 4 && ajax2.status == 200) {
                console.log("uslo u ajax za response");
                let odgovor = JSON.parse(ajax2.response);
                //da bih dobila moze li se dodati boolean varijablu
                let mozeSeDodati = odgovor.split("&");
                console.log(mozeSeDodati[1]);

                Pozivi.ucitajPodatkeIzBaze();
                promjenaForme();
                if (!JSON.parse(mozeSeDodati[1]))
                    alert("Nije moguće rezervisati salu " + salaIzabrana + " za navedeni datum " + danNaKojiJeKliknuoUser + "/" + mjesec + "/" + godina + " i termin od " + poc + " do " + end + "!");
            }
        }
    }
    function ucitajSlikeAjaxImpl() {
        var ajax3 = new XMLHttpRequest();
        console.log("u pozivi.js je");
        if (index === 0)
            ajax3.open("GET", "prve", true);
        else if (index === 1)
            ajax3.open("GET", "druge", true);
        else if (index === 2)
            ajax3.open("GET", "trece", true);
        else if (index === 3)
            ajax3.open("GET", "cetvrte", true);

        ajax3.onreadystatechange = function () {// Anonimna funkcija
            if (ajax3.readyState == 4 && ajax3.status == 200) {
                let data = JSON.parse(ajax3.response);
                kes.push(data);
                let slike = [];
                if (index === 0) {
                    slike = [];
                    slike.push(data.slika1);
                    slike.push(data.slika2);
                    slike.push(data.slika3);
                }
                else if (index === 1) {
                    slike = [];
                    slike.push(data.slika4);
                    slike.push(data.slika5);
                    slike.push(data.slika6);
                }
                else if (index === 2) {
                    slike = [];
                    slike.push(data.slika7);
                    slike.push(data.slika8);
                    slike.push(data.slika9);
                }
                else if (index === 3) {
                    slike = [];
                    slike.push(data.slika10);
                }
                document.getElementById("prva").src = slike[0];
                if (slike.length >= 2) document.getElementById("druga").src = slike[1];
                else {
                    document.getElementById("druga").src = "";
                    document.getElementById("druga").alt = "";
                }
                if (slike.length === 3) document.getElementById("treca").src = slike[2];
                else {
                    document.getElementById("treca").src = "";
                    document.getElementById("treca").alt = "";
                }

            }
        }
        ajax3.send();
    }
    function ucitajOsobljeAjaxImpl() {
        console.log("u ucitaj osoblje");
        //ovdje saljemo ajax zahtjev za podacima o osoblju iz baze podataka
        var ajax4 = new XMLHttpRequest();
        ajax4.open("GET", "osoblje", true);
        ajax4.onreadystatechange = function () {// Anonimna funkcija
            if (ajax4.readyState == 4 && ajax4.status == 200) {
                let data = JSON.parse(ajax4.response);
                console.log("data.length" + data.length);
                //u data bi trebali biti svi objekti iz tabele osoblje u json formaatu
                //data bi trebala biti niz
                for (var i = 0; i < data.length; i++) {
                    var option = document.createElement('option');
                    option.text = option.value = data[i].ime + " " + data[i].prezime + "," + data[i].uloga;
                    document.getElementById("osobljeIzaberi").add(option, 0);
                }
            }
        }

        ajax4.send();

    }
    function upisiUBazuImpl() {
        //ovdje ponovo ucitam podatke iz baze i ucitat ce se u periodicnaGlavna i vanrednaGlavna
        Pozivi.ucitajPodatkeIzBaze();
        //kad se ucitaju ovdje mogu napraviti sve provjere da li se moze dodati u bazu ili ne, tj sve ono sto
        //sam sa zauzeca.json radila na serveru
        let salaIzabrana = document.getElementById("saleIzaberi").value;
        let poc = document.getElementById("pocetak").value;
        let end = document.getElementById("kraj").value;
        let periodicnaChecked = document.getElementById("check").checked;
        let semester;
        let mjesec;
        for (let i = 0; i < 12; i++) {
            if (mjesecUKojemJeUserKliknuo.localeCompare(mjeseciUGodini[i]) === 0) {
                mjesec = i;
                break;
            }
        }
        if (mjesec === 9 || mjesec === 10 || mjesec === 11 || mjesec === 0) semester = "zimski";
        else if (mjesec >= 1 && mjesec <= 5) semester = "ljetni";

        let predavac = document.getElementById("osobljeIzaberi").value;
        let predavacIme = predavac.split(" ")[0];
        let predavacPrezime = predavac.split(" ")[1].split(",")[0];
        let predavacUloga = predavac.split(" ")[1].split(",")[1];

        let mozeSeRezervisati;
        let mozeSeRezervisatiPer = true;
        let mozeSeRezervisatiVan = true;

        for (let i = 0; i < vanrednaGlavna.length; i++) {
            console.log("u vanredna peltji");

            let element = vanrednaGlavna[i];

            //ako je izabrana sala i pronadje se u listi vanrednih
            if (salaIzabrana.trim() != '' && element.naziv.localeCompare(salaIzabrana) === 0) {
                console.log("u sala");

                let danElement = parseInt(element.datum.substring(0, 2), 10);
                let mjesecElement = parseInt(element.datum.substring(3, 5), 10) - 1;
                //ako se i dani poklapaju
                if (danElement === danNaKojiJeKliknuoUser) {
                    //i mjesec
                    console.log("u dan");

                    if (mjesec === mjesecElement) {
                        //i vrijeme se poklapa
                        console.log("u mjesec");

                        if ((element.pocetak >= poc && element.pocetak < end) || (element.kraj > poc && element.kraj <= end) || (poc > element.pocetak && end < element.kraj)) {
                            //onda se ne moze rezervisati
                            console.log("u vrijeme");

                            mozeSeRezervisatiVan = false;
                            console.log(mozeSeRezervisatiVan);

                            //u svakom drugom slucaju moguce je rezervisati termin i ostaje true
                        }
                    }
                }
            }
        }
        //  } else {
        console.log("element dan");

        //provjeravamo periodicna zauzeca na taj dan
        for (let i = 0; i < periodicnaGlavna.length; i++) {
            console.log("u petlji per");

            let element = periodicnaGlavna[i];
            //ako je izabrana sala i pronadje se u listi vanrednih
            if (salaIzabrana.trim() != '' && element.naziv.localeCompare(salaIzabrana) === 0) {
                //ako je izabran dan u istom semestru u kojem vec postoji zauzece
                console.log("u sala");

                if (element.semestar === semester) {
                    console.log("u semestar");
                    console.log("element dan");
                    console.log(element.dan);
                    console.log("dan u sedmici");
                    console.log(danUSedmici);
                    //pa ako je izabran isti dan u sedmici
                    if (element.dan === danUSedmici) {

                        //ako se vremenski intervali poklapaju
                        if ((element.pocetak >= poc && element.pocetak < end) || (element.kraj > poc && element.kraj <= end) || (poc > element.pocetak && end < element.kraj)) {
                            console.log("u vrijeme");

                            mozeSeRezervisatiPer = false;
                            console.log(mozeSeRezervisatiPer);

                        }
                    }
                }
            }
            //   }

        }
        console.log("periodicnaChecked " + periodicnaChecked);
        if (periodicnaChecked) {
            for (let i = 0; i < vanrednaGlavna.length; i++) {
                console.log("u vanredna peltjiii");

                let element = vanrednaGlavna[i];

                //ako je izabrana sala i pronadje se u listi vanrednih
                if (salaIzabrana.trim() != '' && element.naziv.localeCompare(salaIzabrana) === 0) {
                    console.log("u salaaaa");

                    let danElement = parseInt(element.datum.substring(0, 2), 10);
                    let mjesecElement = parseInt(element.datum.substring(3, 5), 10);
                    console.log("semestar: " + semester);
                    console.log("mjesecElement " + mjesecElement);
                    if ((semester === "ljetni" && (mjesecElement >= 2 && mjesecElement <= 6)) || (semester === "zimski" && (mjesecElement === 10 || mjesecElement === 11 || mjesecElement === 12 || mjesecElement === 1))) {
                        //ako se i dani poklapaju
                        let dayString = 2019 + "-" + mjesecElement + "-" + danElement;

                        let danUSedmiciVanredni = (new Date(dayString)).getDay() - 1;
                        if (danUSedmiciVanredni < 0) danUSedmiciVanredni = 6;
                        console.log("dan u sedmici vanredni ");
                        console.log(danUSedmiciVanredni);
                        if (danUSedmiciVanredni === danUSedmici) {
                            mozeSeRezervisatiPer = false;
                            break;
                        }
                    }
                }
            }
        }

        if (mozeSeRezervisatiPer == true && mozeSeRezervisatiVan == true) mozeSeRezervisati = true;
        else mozeSeRezervisati = false;
        let terminZauzeca;
        let datumZaJson;
        let predavacIzListi;
        mjesec = mjesec + 1;
        let godinaa = 2019;
        let dat, mj;
        if (danNaKojiJeKliknuoUser < 10) dat = "0" + danNaKojiJeKliknuoUser;
        else dat = danNaKojiJeKliknuoUser;
        if (mjesec < 10) mj = "0" + mjesec;
        else mj = mjesec;
        datumZaJson = "" + dat + "." + mj + "." + godinaa;
        console.log("datumZaJson " + datumZaJson);
        console.log("danUSedmici " + danUSedmici);
        console.log("semestar " + semester);
        if (mozeSeRezervisati) {
            //ide kod za kreiranje objekta i slanje post za upis u zauzeca.json


            if (!periodicnaChecked) {

                terminZauzeca = { redovni: false, dan: null, datum: datumZaJson, semestar: null, pocetak: poc, kraj: end, naziv: salaIzabrana, predavac: predavacIme + " " + predavacPrezime };
                //vanrednaJSON.push(terminZauzeca);
            } else {
                terminZauzeca = { redovni: true, dan: danUSedmici, datum: null, semestar: semester, pocetak: poc, kraj: end, naziv: salaIzabrana, predavac: predavacIme + " " + predavacPrezime };
                //periodicnaJSON.push(terminZauzeca);
            }
            var ajax2 = new XMLHttpRequest();

            //ovdje treba samo open i send
            ajax2.open("POST", "upisiUBazu", true);
            ajax2.setRequestHeader("Content-Type", "application/json");
            console.log(JSON.stringify(terminZauzeca));
            ajax2.send(JSON.stringify(terminZauzeca));
            ajax2.onreadystatechange = function () {
                if (ajax2.readyState == 4 && ajax2.status == 200) {
                    console.log("uslo u ajax za response");
                    let mozeSeRezervisatiServer = JSON.parse(ajax2.response);
                    //Pozivi.ucitajPodatkeIzBaze();
                    console.log(mozeSeRezervisatiServer);
                    if(mozeSeRezervisatiServer){
                    alert("Uspjesno ste rezervisali salu");
                    promjenaForme();
                }else{
                    alert("Nije moguće rezervisati salu " + salaIzabrana + " za navedeni datum " + danNaKojiJeKliknuoUser + "/" + mjesec + "/" + godina + " i termin od " + poc + " do " + end + ", jer ju je rezervisao " + predavacUloga + " " + predavacIme + " " + predavacPrezime + "!");
                }
                }
            }
        } else {
            //alert sa porukom da se ne moze rezervisati taj termin
            //treba mi iz baze osoba koja je rezervisala tu salu a ne sa forme, tj iz liste per ili van
            let predavacImeLista;
            let predavacPrezimeLista;
            let predavacUlogaLista;

            for (var i = 0; i < vanrednaGlavna.length; i++) {
                var el = vanrednaGlavna[i];
                predavacIzListi = el.predavac.split(" ");
                console.log(predavacIzListi);
                if (el.naziv.localeCompare(salaIzabrana) === 0) {
                    console.log("vanredna izabrana sala");
                    console.log(el.datum);
                    console.log(datumZaJson);
                    if(periodicnaChecked){
                        let danElement = parseInt(el.datum.substring(0, 2), 10);
                        let mjesecElement = parseInt(el.datum.substring(3, 5), 10);
                        console.log("semestar: " + semester);
                        console.log("mjesecElement " + mjesecElement);
                        if ((semester === "ljetni" && (mjesecElement >= 2 && mjesecElement <= 6)) || (semester === "zimski" && (mjesecElement === 10 || mjesecElement === 11 || mjesecElement === 12 || mjesecElement === 1))) {
                            //ako se i dani poklapaju
                            let dayString = 2019 + "-" + mjesecElement + "-" + danElement;
    
                            let danUSedmiciVanredni = (new Date(dayString)).getDay() - 1;
                            if (danUSedmiciVanredni < 0) danUSedmiciVanredni = 6;
                            console.log("dan u sedmici vanredni ");
                            console.log(danUSedmiciVanredni);
                            if (danUSedmiciVanredni === danUSedmici) {
                                predavacImeLista = predavacIzListi[0];
                            predavacPrezimeLista = predavacIzListi[1];
                            predavacUlogaLista = predavacIzListi[2];
                                break;
                            }
                        }




                    }
                    else{
                    if (el.datum.toString().localeCompare(datumZaJson) === 0) {
                        console.log("vanredna datum");
                        if ((el.pocetak >= poc && el.pocetak < end) || (el.kraj > poc && el.kraj <= end) || (poc > el.pocetak && end < el.kraj)) {
                            console.log("vijeme vanredna");
                            predavacImeLista = predavacIzListi[0];
                            predavacPrezimeLista = predavacIzListi[1];
                            predavacUlogaLista = predavacIzListi[2];
                            break;
                        }
                    }
                }
                }
            }
            console.log(periodicnaGlavna);
            console.log(vanrednaGlavna);
            for (var i = 0; i < periodicnaGlavna.length; i++) {
                var el = periodicnaGlavna[i];
                predavacIzListi = el.predavac.split(" ");
                console.log(predavacIzListi);
                if (el.naziv.localeCompare(salaIzabrana) === 0) {
                    console.log("periodicna sala izabrana")
                    if (el.dan === danUSedmici && el.semestar.localeCompare(semester) === 0) {
                        console.log("periodicna dan i semestar");
                        if ((el.pocetak >= poc && el.pocetak < end) || (el.kraj > poc && el.kraj <= end) || (poc > el.pocetak && end < el.kraj)) {
                            console.log("periodicna poc i kraj");
                            predavacImeLista = predavacIzListi[0];
                            predavacPrezimeLista = predavacIzListi[1];
                            predavacUlogaLista = predavacIzListi[2];
                            //alert("Nije moguće rezervisati salu " + salaIzabrana + " za navedeni datum " + danNaKojiJeKliknuoUser + "/" + mjesec + "/" + godina + " i termin od " + poc + " do " + end + ", jer ju je rezervisao " + predavacUlogaLista + " " + predavacImeLista + " " + predavacPrezimeLista + "!");
                            break;
                        }
                    }
                }
            }

            alert("Nije moguće rezervisati salu " + salaIzabrana + " za navedeni datum " + danNaKojiJeKliknuoUser + "/" + mjesec + "/" + godina + " i termin od " + poc + " do " + end + ", jer ju je rezervisao " + predavacUlogaLista + " " + predavacImeLista + " " + predavacPrezimeLista + "!");
        }
    }

    function ucitajOsobeISaleImpl() {
        //saljem zahtjev za podacima gdje je koji predavac u datom trenutku

        var ajax4 = new XMLHttpRequest();
        ajax4.open("GET", "osobeISale", true);
        ajax4.onreadystatechange = function () {// Anonimna funkcija
            if (ajax4.readyState == 4 && ajax4.status == 200) {
                //u response se nalazi spisak sala i osoba koje treba upisati u tabelu
                let data = JSON.parse(ajax4.response);
                //kada dobijem te podatke pozovem fju iz osoblje1.js koja ce te podatke da upise u tabele
                napraviTabelu(data.lista);
            }
        }
        ajax4.send();
    }

    return {
        //ime fje: imefjeImpl,
        ucitajZauzecaAjax: ucitajZauzecaAjaxImpl,
        upisiUZauzecaJson: upisiUZauzecaJsonImpl,
        ucitajSlikeAjax: ucitajSlikeAjaxImpl,
        ucitajOsobljeAjax: ucitajOsobljeAjaxImpl,
        ucitajPodatkeIzBaze: ucitajPodatkeIzBazeImpl,
        upisiUBazu: upisiUBazuImpl,
        ucitajOsobeISale: ucitajOsobeISaleImpl
    }
}());
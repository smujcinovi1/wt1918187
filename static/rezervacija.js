let periodicnaChecked;
//ovdje fja koju hocemo
//kopirala sam skoro sve iz kalendar.js
//prvo pozivam fju koja ucitava podatke sa servera
Pozivi.ucitajPodatkeIzBaze();
Pozivi.ucitajOsobljeAjax();
Kalendar.iscrtajKalendar(document.getElementById("calendar-body"), mjesecTrenutni);

//funkcija koja poziva obojiZauzeca svaki put kada se promijeni nesto na formi iznad kalendara
function promjenaForme() {
    let mjesecPrikazani = document.getElementById("nazivMjeseca").textContent;
    let brojTrenutnogMjeseca;
    for (let i = 0; i < 12; i++) {
        if (mjesecPrikazani.localeCompare(mjeseciUGodini[i]) === 0) {
            brojTrenutnogMjeseca = i;
            break;
        }
    }
    console.log(brojTrenutnogMjeseca);
    let salaIzabrana = document.getElementById("saleIzaberi").value;
    console.log(salaIzabrana);
    let poc = document.getElementById("pocetak").value;
    let end = document.getElementById("kraj").value;
    periodicnaChecked = document.getElementById("check").checked;
    console.log(periodicnaChecked);
    //Pozivi.ucitajZauzecaAjax();
    Pozivi.ucitajPodatkeIzBaze();
    Kalendar.iscrtajKalendar(document.getElementById("calendar-body"), brojTrenutnogMjeseca);
    Kalendar.obojiZauzeca(document.getElementById("calendar-body"), brojTrenutnogMjeseca, salaIzabrana, poc, end);
}

function sljedeci() {
    godina = 2019;
    let mjesecPrikazani = document.getElementById("nazivMjeseca").textContent;
    let brojTrenutnogMjeseca;
    for (let i = 0; i < 12; i++) {
        if (mjesecPrikazani.localeCompare(mjeseciUGodini[i]) === 0) {
            brojTrenutnogMjeseca = i;
            break;
        }
    }
    //koristimo brojTrenutnogMjeseca jer nam treba sljedbenik od trenutno prikazanog mjeseca
    if (brojTrenutnogMjeseca + 1 <= 11) {
        brojTrenutnogMjeseca++;
        Kalendar.iscrtajKalendar(document.getElementById("calendar-body"), brojTrenutnogMjeseca);

        let salaIzabrana = document.getElementById("saleIzaberi").value;
        let poc = document.getElementById("pocetak").value;
        let end = document.getElementById("kraj").value;
        Kalendar.obojiZauzeca(document.getElementById("calendar-body"), brojTrenutnogMjeseca, salaIzabrana, poc, end);
    } else {
        document.getElementById("dva").disabled = true;
    }
}

function prethodni() {
    godina = 2019;
    let mjesecPrikazani = document.getElementById("nazivMjeseca").textContent;
    let brojTrenutnogMjeseca;
    for (let i = 0; i < 12; i++) {
        if (mjesecPrikazani.localeCompare(mjeseciUGodini[i]) === 0) {
            brojTrenutnogMjeseca = i;
            break;
        }
    }
    //koristimo brojTrenutnogMjeseca jer nam treba prethodnik od trenutno prikazanog mjeseca
    if (brojTrenutnogMjeseca - 1 >= 0) {
        brojTrenutnogMjeseca--;
        Kalendar.iscrtajKalendar(document.getElementById("calendar-body"), brojTrenutnogMjeseca);
        let salaIzabrana = document.getElementById("saleIzaberi").value;
        let poc = document.getElementById("pocetak").value;
        let end = document.getElementById("kraj").value;
        Kalendar.obojiZauzeca(document.getElementById("calendar-body"), brojTrenutnogMjeseca, salaIzabrana, poc, end);
    } else {
        document.getElementById("jedan").disabled = true;
    }
}

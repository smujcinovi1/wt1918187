const supertest = require("supertest");
const assert = require('assert');
const app = require("./index");
const db = require('./db.js');

  describe("GET /osoblje", function () {
    it("status code treba biti 200 za ucitavanje osoblja", function (done) {
      supertest(app)
        .get("/osoblje")
        .expect(200)
        .end(function (err, res) {
          if (err) done(err);
          done();
        });
    });

    it("da li smo dobili sve podatke iz tabele osoblje", function (done) {
      supertest(app)
        .get("/osoblje")
        .expect([
          { id: 1, ime: 'Neko', prezime: 'Nekic', uloga: 'profesor' },
          { id: 2, ime: 'Drugi', prezime: 'Neko', uloga: 'asistent' },
          { id: 3, ime: 'Test', prezime: 'Test', uloga: 'asistent' }
        ])
        .end(function (err, res) {
          if (err) done(err);
          done();
        });
    });
  });

  describe("GET /sale", function () {
    it("status code treba biti 200 za ucitavanje sala", function (done) {
      supertest(app)
        .get("/sale")
        .expect(200)
        .end(function (err, res) {
          if (err) done(err);
          done();
        });
    });
    it("provjera da li smo dobili sve podatke iz tabele sale", function (done) {
    supertest(app)
    .get("/sale")
    .expect([
      { id: 1, naziv:'1-11',zaduzenaOsoba:1 },
      { id: 2, naziv:'1-15',zaduzenaOsoba:2},
      { id: 3, naziv:'1-11',zaduzenaOsoba:3 }
    ])
    .end(function (err, res) {
      if (err) done(err);
      done();
    });
  });
  
});

describe("GET /podaciIzBaze", function () {
  it("status code treba biti 200 za dobijanje podataka iz baze", function (done) {
    supertest(app)
      .get("/podaciIzBaze")
      .expect(200)
      .end(function (err, res) {
        if (err) done(err);
        done();
      });
  });

  it("da li smo prilikom ucitavanja stranice dobili sve rezervacije iz baze", function (done) {
    supertest(app)
      .get("/podaciIzBaze")
      .expect('{"periodicna":[{"dan":0,"semestar":"zimski","pocetak":"13:00:00","kraj":"14:00:00","naziv":"1-11","predavac":"Test Test asistent"}],"vanredna":[{"datum":"01.01.2019","pocetak":"12:00:00","kraj":"13:00:00","naziv":"1-11","predavac":"Neko Nekic profesor"}]}')
      .end(function (err, res) {
        if (err) done(err);
        done();
      });
  });
});

  describe("POST /upisiUBazu", function () {
    it("status code treba biti 200 za upis u bazu", function (done) {
      supertest(app)
      .post("/upisiUBazu")
      .send({ redovni: true, dan: 5, datum: null, semestar: "ljetni", pocetak: "11:00:00", kraj: "15:00:00", naziv: "VA1", predavac: "Test Test"})
      .expect(200)
      .end(function(err, res){
        if (err) done(err);
        //setTimeout(function(){
          //console.log('waiting over.');
          done();
      //}, 3000);
      });
    });

  });
  describe("provjera da li je upisano", function () {

    it("provjera da li se rezervacija upisala u bazu", function (done) {
      supertest(app)
        .get("/podaciIzBaze")
        .expect('{"periodicna":[{"dan":0,"semestar":"zimski","pocetak":"13:00:00","kraj":"14:00:00","naziv":"1-11","predavac":"Test Test asistent"},{"dan":5,"semestar":"ljetni","pocetak":"11:00:00","kraj":"15:00:00","naziv":"VA1","predavac":"Test Test asistent"}],"vanredna":[{"datum":"01.01.2019","pocetak":"12:00:00","kraj":"13:00:00","naziv":"1-11","predavac":"Neko Nekic profesor"}]}')
        .end(function (err, res) {
          if (err) done(err);
         // setTimeout(function(){
          //  console.log('waiting over.');
            done();
       // }, 3000);
        });
    });
  });
  
  describe("pokusaj upisivanja rezervacije koja se poklapa sa nekom postojecom", function () {
    //jos test provjere da li je upisana sala ako je vec zauzeta ranije i provjera da li se doda ako  se samo promijeni osoba
    //a sala ranije zauzeta od neke druge osobe
    it("status code treba biti 200 za pokusaj upisivanja postojece rezervacije", function (done) {
      supertest(app)
      .post("/upisiUBazu")
      .send({ redovni: false, dan: null, datum: "07.01.2019", semestar: null, pocetak: "09:00:00", kraj: "14:00:00", naziv: "1-11", predavac: "Test Test"})
      .expect(200)
      .end(function(err, res){
        if (err) done(err);
       // setTimeout(function(){
        //  console.log('waiting over.');
          done();
      //}, 3000);
      });
    });
  });
  describe("provjera da li se prethodna rezervacija upisala u bazu", function () {
    it("provjera da li se rezervacija upisala u bazu", function (done) {
      supertest(app)
        .get("/podaciIzBaze")
        .expect('{"periodicna":[{"dan":0,"semestar":"zimski","pocetak":"13:00:00","kraj":"14:00:00","naziv":"1-11","predavac":"Test Test asistent"},{"dan":5,"semestar":"ljetni","pocetak":"11:00:00","kraj":"15:00:00","naziv":"VA1","predavac":"Test Test asistent"}],"vanredna":[{"datum":"01.01.2019","pocetak":"12:00:00","kraj":"13:00:00","naziv":"1-11","predavac":"Neko Nekic profesor"}]}')
        .end(function (err, res) {
          if (err) done(err);
        //  setTimeout(function(){
         //   console.log('waiting over.');
            done();
        //}, 3000);
        });
      });
    });

    describe("pokusaj drugog predavaca da rezervise salu koja je zauzeta", function () {

      it("status code treba biti 200 za pokusaj upisivanja postojece rezervacije samo druga osoba", function (done) {
        supertest(app)
        .post("/upisiUBazu")
        .send({ redovni: false, dan: null, datum: "07.01.2019", semestar: null, pocetak: "09:00:00", kraj: "14:00:00", naziv: "1-11", predavac: "Drugi Neko"})
        .expect(200)
        .end(function(err, res){
          if (err) done(err);
        //  setTimeout(function(){
        //    console.log('waiting over.');
            done();
        //}, 3000);
        });
      });
    });


    describe("provjera da li se prethodna rezervacija upisala u bazu", function () {

      it("provjera da li se rezervacija upisala u bazu", function (done) {
        supertest(app)
          .get("/podaciIzBaze")
          .expect('{"periodicna":[{"dan":0,"semestar":"zimski","pocetak":"13:00:00","kraj":"14:00:00","naziv":"1-11","predavac":"Test Test asistent"},{"dan":5,"semestar":"ljetni","pocetak":"11:00:00","kraj":"15:00:00","naziv":"VA1","predavac":"Test Test asistent"}],"vanredna":[{"datum":"01.01.2019","pocetak":"12:00:00","kraj":"13:00:00","naziv":"1-11","predavac":"Neko Nekic profesor"}]}')
          .end(function (err, res) {
            if (err) done(err);
            //setTimeout(function(){
            //  console.log('waiting over.');
              done();
          //}, 3000);
          });
        });

});
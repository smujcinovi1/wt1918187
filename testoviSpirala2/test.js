let assert = chai.assert;
describe('Kalendar', function() {
    describe('iscrtajKalendar(kalendarRef, mjesec)', function() {
        it('Septembar ima 30 dana', function(){
            Kalendar.iscrtajKalendar(document.getElementById("calendar-body"),8);
            var dani = document.getElementById("calendar-body").querySelectorAll(".tabelaCelija");
            var brojDana = dani.length;
            assert.equal(brojDana, 30, "Neispravan broj dana- treba 30 dana");
        });

        it('Decembar ima 31 dan', function(){
            Kalendar.iscrtajKalendar(document.getElementById("calendar-body"),11);
            var dani = document.getElementById("calendar-body").querySelectorAll(".tabelaCelija");
            var brojDana = dani.length;
            assert.equal(brojDana, 31, "Neispravan broj dana- treba 31 dan");
        });

        it('Prvi dan u novembru je petak', function(){
            Kalendar.iscrtajKalendar(document.getElementById("calendar-body"),10);
            var prviRed = document.getElementById("calendar-body").querySelector("tr");
            var kolone = prviRed.getElementsByTagName("td");
            let prviDan = (new Date(godina, mjesecTrenutni)).getDay();
            if(prviDan !=0) prviDan--;
            else prviDan = 6;
            assert.equal(kolone[prviDan].querySelector(".datum").innerText, 1, "Nije prvi dan petak");
        });

        it('Posljednji dan u novembru je subota', function(){
            Kalendar.iscrtajKalendar(document.getElementById("calendar-body"),10);
            var tabela = document.getElementsByClassName("kalendarTabela")[0];
            var redovi = tabela.rows;
            var posljednjiRed = redovi[redovi.length-1];
            if(posljednjiRed.childElementCount === 0){
                posljednjiRed = redovi[redovi.length-2];
            }
            var posljednjiDanUMjesecu = (new Date(godina, mjesecTrenutni+1,0)).getDay();
            if(posljednjiDanUMjesecu != 0) posljednjiDanUMjesecu--;
            else posljednjiDanUMjesecu = 6;
            assert.equal(posljednjiDanUMjesecu, 5, "nije subota posljednji dan");
            assert.equal(posljednjiDanUMjesecu, posljednjiRed.childElementCount-1, "Subota nije posljednji dan");
        });
        it('Dani u januaru od 1 do 31', function(){
            Kalendar.iscrtajKalendar(document.getElementById("calendar-body"),0);
            //prvo provjerimo da li je prvi dan utorak, tj da li mjesec pocinje od utorka
            var prviRed = document.getElementById("calendar-body").querySelector("tr");
            var kolone = prviRed.getElementsByTagName("td");
            let prviDan = (new Date(godina, 0)).getDay();
            if(prviDan !=0) prviDan--;
            else prviDan = 6;
            assert.equal(kolone[prviDan].querySelector(".datum").innerText, 1, "Nije prvi dan utorak");

            //uzimamo sve celije
            var tabela = document.getElementsByClassName("kalendarTabela")[0];
            //u redovi se nalaze svi redovi iz tabele
            var redovi = tabela.rows;
            //brojac sluzi za poredjenje sa datumom
            let brojac = 1;
            //vanjska petlja sluzi da prolazimo kroz svaki red posebno
            for(let i=0; i < redovi.length;i++){
                //iz svakog reda izdvajamo kolone
                var kolone = redovi[i].cells;
                //unutrasnjom petljom prolazimo kroz svaku celiju tabele
                for(let j=0; j<kolone.childElementCount;j++){
                    //uslov je postavljen jer ne pocinje svaki mjesec od ponedjeljka, tako da ce neke celije biti prazne
                     if(kolone[j] != 0){
                        assert.equal(kolone[j].querySelector(".datum").innerText, brojac, "Nisu dobri datumi");
                        brojac++;
                     }
                }
            }
           });

           //jos dva moja testa
           it('Postavi se odgovarajuci naziv mjeseca na vrhu kalendara', function(){
                for(let i = 0; i<12;i++){
                    Kalendar.iscrtajKalendar(document.getElementById("calendar-body"),i);
                    assert.equal(document.getElementById("nazivMjeseca").textContent, mjeseciUGodini[i], "Nije dobar naziv mjeseca");
                }
           });

           it('Februar(2019) ima 28 dana', function(){
                Kalendar.iscrtajKalendar(document.getElementById("calendar-body"),1);
                let brojDana = document.querySelectorAll("body>div>div>table>tbody>tr>td>tr").length/2;
                assert.equal(brojDana, 28, "Neispravan broj dana");
            });
           


    });

    describe('obojiZauzeca(kalendarRef, mjesec, sala, pocetak, kraj)', function() {
        
        it('Ne boji zauzeca kada nisu ucitani podaci', function(){
            //postavljeno za novembar, mada se umjesto "10" moze staviti varijabla "mjesecTrenutni" pa da ispituje za mjesec koji je stvarno po kalendaru
            Kalendar.ucitajPodatke([],[]);
            let prviDanUMjesecu = (new Date(godina, 10,1)).getDay();
            if(prviDanUMjesecu !=0){
                prviDanUMjesecu = prviDanUMjesecu-1;
            }
            else{
                prviDanUMjesecu = 6;
            }
            Kalendar.obojiZauzeca(document.getElementById("calendar-body"), 10, "0-02", "13:00","15:00");
            for(let i=prviDanUMjesecu;i<document.querySelectorAll("body>div>div>table>tbody>tr>td").length;i++){
               assert.equal(document.querySelectorAll("body>div>div>table>tbody>tr>td")[i].querySelectorAll("tr")[1].querySelector("th").getAttribute("class"),"slobodna", "Pada test!" );
            }
        });
        it('Test za duple vrijednosti zauzeca - oboji zauzeca iako postoje duple vrijednosti', function(){
            Kalendar.ucitajPodatke([{dan:3,semestar:"ljetni",pocetak:"13:00",kraj:"16:00",naziv:"0-02",predavac:"prof1"},
                        {dan:2,semestar:"zimski",pocetak:"12:30",kraj:"14:30",naziv:"0-09",predavac:"prof2"},
                        {dan:2,semestar:"zimski",pocetak:"12:30",kraj:"14:30",naziv:"0-07",predavac:"prof2"},
                        {dan:1,semestar:"ljetni",pocetak:"15:00",kraj:"17:00",naziv:"0-03",predavac:"prof3"},
                        {dan:6,semestar:"zimski",pocetak:"11:00",kraj:"13:00",naziv:"MA",predavac:"prof4"}],[{datum:"18.10.2019",pocetak:"15:00",kraj:"19:00",naziv:"0-02",predavac:"prof2"}]);

            Kalendar.iscrtajKalendar(document.getElementById("calendar-body"),10);
            Kalendar.obojiZauzeca(document.getElementById("calendar-body"), 10, "0-09", "12:30","14:30");
           // Kalendar.obojiZauzeca(document.getElementById("calendar-body"), 10, "0-07", "12:30","14:30");
            let k = 9;
            for(let i=4;i<document.querySelectorAll("body>div>div>table>tbody>tr>td").length;i++){
                if(i === k){
                    assert.equal(document.querySelectorAll("body>div>div>table>tbody>tr>td")[i].querySelectorAll("tr")[1].querySelector("th").getAttribute("class"),"zauzeta", "Pada test!" );
                    k+=7;
                }
            }
        });

        it('Nemoj obojiti periodicno zauzece koje je u sljeedecem semestru', function(){
            
            Kalendar.ucitajPodatke([{dan:3,semestar:"ljetni",pocetak:"13:00",kraj:"16:00",naziv:"0-02",predavac:"prof1"}],[{datum:"18.10.2019",pocetak:"12:30",kraj:"14:30",naziv:"0-02",predavac:"prof2"}]);

            Kalendar.iscrtajKalendar(document.getElementById("calendar-body"),10);
            Kalendar.obojiZauzeca(document.getElementById("calendar-body"), 10, "0-02", "12:00","17:30");
            for(let i=4;i<document.querySelectorAll("body>div>div>table>tbody>tr>td").length;i++){
                    assert.equal(document.querySelectorAll("body>div>div>table>tbody>tr>td")[i].querySelectorAll("tr")[1].querySelector("th").getAttribute("class"),"slobodna", "Pada test!" ); 
            }

        });
        it('Nemoj bojiti termin koji je u drugom mjesecu', function(){
            //postoji termin u oktobru zauzet, ne treba se obojiti u novembru
            Kalendar.ucitajPodatke([{dan:0,semestar:"ljetni",pocetak:"13:00",kraj:"16:00",naziv:"0-01",predavac:"prof1"}],[{datum:"18.10.2019",pocetak:"12:30",kraj:"14:30",naziv:"0-02",predavac:"prof2"}]);

            Kalendar.iscrtajKalendar(document.getElementById("calendar-body"),10);
            Kalendar.obojiZauzeca(document.getElementById("calendar-body"), 10, "0-02", "12:00","15:00");
            for(let i=4;i<document.querySelectorAll("body>div>div>table>tbody>tr>td").length;i++){
                    assert.equal(document.querySelectorAll("body>div>div>table>tbody>tr>td")[i].querySelectorAll("tr")[1].querySelector("th").getAttribute("class"),"slobodna", "Pada test!" ); 
            }
            //provjera da li je obojen  u oktobru
            Kalendar.iscrtajKalendar(document.getElementById("calendar-body"),9);
            Kalendar.obojiZauzeca(document.getElementById("calendar-body"), 9, "0-02", "12:30","14:30");
            assert.equal(document.querySelectorAll("body>div>div>table>tbody>tr>td")[18].querySelectorAll("tr")[1].querySelector("th").getAttribute("class"),"zauzeta", "Pada test!" ); 

        });
        it('Svi termini u mjesecu zauzeti', function(){
            //postoji termin u oktobru zauzet, ne treba se obojiti u novembru
            Kalendar.ucitajPodatke([{dan:0,semestar:"zimski",pocetak:"13:00",kraj:"16:00",naziv:"0-01",predavac:"prof1"},
                                    {dan:1,semestar:"zimski",pocetak:"13:00",kraj:"16:00",naziv:"0-02",predavac:"prof1"},
                                    {dan:2,semestar:"zimski",pocetak:"13:00",kraj:"16:00",naziv:"0-03",predavac:"prof1"},
                                    {dan:3,semestar:"zimski",pocetak:"13:00",kraj:"16:00",naziv:"0-04",predavac:"prof1"},
                                    {dan:4,semestar:"zimski",pocetak:"13:00",kraj:"16:00",naziv:"0-05",predavac:"prof1"},
                                    {dan:5,semestar:"zimski",pocetak:"13:00",kraj:"16:00",naziv:"0-06",predavac:"prof1"},
                                    {dan:6,semestar:"zimski",pocetak:"13:00",kraj:"16:00",naziv:"0-07",predavac:"prof1"}],[{datum:"18.06.2019",pocetak:"12:30",kraj:"14:30",naziv:"0-02",predavac:"prof2"}]);

            Kalendar.iscrtajKalendar(document.getElementById("calendar-body"),10);
            Kalendar.obojiZauzeca(document.getElementById("calendar-body"), 10, "0-01", "12:00","15:00");
            Kalendar.obojiZauzeca(document.getElementById("calendar-body"), 10, "0-02", "12:00","15:00");
            Kalendar.obojiZauzeca(document.getElementById("calendar-body"), 10, "0-03", "12:00","15:00");
            Kalendar.obojiZauzeca(document.getElementById("calendar-body"), 10, "0-04", "12:00","15:00");
            Kalendar.obojiZauzeca(document.getElementById("calendar-body"), 10, "0-05", "12:00","15:00");
            Kalendar.obojiZauzeca(document.getElementById("calendar-body"), 10, "0-06", "12:00","15:00");
            Kalendar.obojiZauzeca(document.getElementById("calendar-body"), 10, "0-07", "12:00","15:00");

            for(let i=4;i<document.querySelectorAll("body>div>div>table>tbody>tr>td").length;i++){
                    assert.equal(document.querySelectorAll("body>div>div>table>tbody>tr>td")[i].querySelectorAll("tr")[1].querySelector("th").getAttribute("class"),"zauzeta", "Pada test!" ); 
            }
           
        });
        it('Dva puta uzastopno pozivanje obojiZauzeca', function(){
            Kalendar.ucitajPodatke([{dan:3,semestar:"ljetni",pocetak:"13:00",kraj:"16:00",naziv:"0-02",predavac:"prof1"},
                        {dan:2,semestar:"zimski",pocetak:"12:30",kraj:"14:30",naziv:"0-09",predavac:"prof2"},
                        {dan:1,semestar:"ljetni",pocetak:"15:00",kraj:"17:00",naziv:"0-03",predavac:"prof3"},
                        {dan:6,semestar:"zimski",pocetak:"11:00",kraj:"13:00",naziv:"MA",predavac:"prof4"}],[{datum:"18.10.2019",pocetak:"15:00",kraj:"19:00",naziv:"0-02",predavac:"prof2"}]);

            Kalendar.iscrtajKalendar(document.getElementById("calendar-body"),10);
            Kalendar.obojiZauzeca(document.getElementById("calendar-body"), 10, "0-09", "12:00","15:00");
            Kalendar.obojiZauzeca(document.getElementById("calendar-body"), 10, "0-09", "12:00","15:00");
            let k = 9;
            for(let i=4;i<document.querySelectorAll("body>div>div>table>tbody>tr>td").length;i++){
                if(i === k){
                    assert.equal(document.querySelectorAll("body>div>div>table>tbody>tr>td")[i].querySelectorAll("tr")[1].querySelector("th").getAttribute("class"),"zauzeta", "Pada test!" );
                    k+=7;
                }
            }


        });
        

        it('Primjenjuju se samo posljednji ucitani podaci', function(){

            Kalendar.ucitajPodatke([{dan:3,semestar:"ljetni",pocetak:"13:00",kraj:"16:00",naziv:"0-02",predavac:"prof1"},
                        {dan:2,semestar:"zimski",pocetak:"12:30",kraj:"14:30",naziv:"0-09",predavac:"prof2"},
                        {dan:1,semestar:"ljetni",pocetak:"15:00",kraj:"17:00",naziv:"0-03",predavac:"prof3"}],[{datum:"18.10.2019",pocetak:"15:00",kraj:"19:00",naziv:"0-02",predavac:"prof2"}]);

            Kalendar.obojiZauzeca(document.getElementById("calendar-body"), 10, "0-09", "12:00","15:00");

            Kalendar.ucitajPodatke([{dan:3,semestar:"ljetni",pocetak:"13:00",kraj:"16:00",naziv:"0-02",predavac:"prof1"},
                        {dan:6,semestar:"zimski",pocetak:"11:00",kraj:"13:00",naziv:"MA",predavac:"prof4"}],[{datum:"18.10.2019",pocetak:"15:00",kraj:"19:00",naziv:"0-02",predavac:"prof2"}]);
            
            Kalendar.obojiZauzeca(document.getElementById("calendar-body"), 10, "MA", "10:00","12:00");

            //provjera da li su ostali prethodni 
            let k = 9;
            for(let i=4;i<document.querySelectorAll("body>div>div>table>tbody>tr>td").length;i++){
                if(i === k){
                    assert.equal(document.querySelectorAll("body>div>div>table>tbody>tr>td")[i].querySelectorAll("tr")[1].querySelector("th").getAttribute("class"),"slobodna", "Pada test!" );
                    k+=7;
                }
            }

            k=6;
            for(let i=4;i<document.querySelectorAll("body>div>div>table>tbody>tr>td").length;i++){
                if(i === k){
                    assert.equal(document.querySelectorAll("body>div>div>table>tbody>tr>td")[i].querySelectorAll("tr")[1].querySelector("th").getAttribute("class"),"zauzeta", "Pada test!" );
                    k+=7;
                }
            }
        });

        //dva moja testa
        it('Da li su obojena i periodicna zauzeca i vanredna zauzeca(ako ih ima) u tom mjesecu', function(){

            Kalendar.ucitajPodatke([{dan:3,semestar:"zimski",pocetak:"13:00",kraj:"16:00",naziv:"0-02",predavac:"prof1"},
                        {dan:2,semestar:"zimski",pocetak:"12:30",kraj:"14:30",naziv:"0-09",predavac:"prof2"},
                        {dan:1,semestar:"ljetni",pocetak:"15:00",kraj:"17:00",naziv:"0-03",predavac:"prof3"},
                        {dan:6,semestar:"zimski",pocetak:"11:00",kraj:"13:00",naziv:"MA",predavac:"prof4"}],[{datum:"23.11.2019",pocetak:"13:00",kraj:"19:00",naziv:"0-02",predavac:"prof2"}]);
            
            Kalendar.obojiZauzeca(document.getElementById("calendar-body"), 10, "0-02", "12:00","15:00");
            //provjerimo za periodicna
            let k=10;
            for(let i=4;i<document.querySelectorAll("body>div>div>table>tbody>tr>td").length;i++){
                if(i === k){
                    assert.equal(document.querySelectorAll("body>div>div>table>tbody>tr>td")[i].querySelectorAll("tr")[1].querySelector("th").getAttribute("class"),"zauzeta", "Pada test!" );
                    k+=7;
                }
            }
            //perovjerimo za vanredno zauzece
            assert.equal(document.querySelectorAll("body>div>div>table>tbody>tr>td")[26].querySelectorAll("tr")[1].querySelector("th").getAttribute("class"),"zauzeta", "Pada test!" ); 

            
        });

        it('Ne oboji se kad se vremena na formi i vrijeme zauzetosti sale NE poklapaju', function(){
            Kalendar.ucitajPodatke([{dan:6,semestar:"zimski",pocetak:"13:00",kraj:"16:00",naziv:"0-02",predavac:"prof1"},
            {dan:2,semestar:"zimski",pocetak:"12:30",kraj:"14:30",naziv:"0-09",predavac:"prof2"},
            {dan:1,semestar:"ljetni",pocetak:"15:00",kraj:"17:00",naziv:"0-03",predavac:"prof3"},
            {dan:6,semestar:"zimski",pocetak:"11:00",kraj:"13:00",naziv:"MA",predavac:"prof4"}],[{datum:"23.11.2019",pocetak:"13:00",kraj:"19:00",naziv:"0-02",predavac:"prof2"},{datum:"18.11.2019",pocetak:"13:00",kraj:"19:00",naziv:"0-02",predavac:"prof2"}]);

            Kalendar.obojiZauzeca(document.getElementById("calendar-body"), 10, "0-02", "09:00","11:00");
            let k=6;
            for(let i=4;i<document.querySelectorAll("body>div>div>table>tbody>tr>td").length;i++){
                if(i === k){
                    assert.equal(document.querySelectorAll("body>div>div>table>tbody>tr>td")[i].querySelectorAll("tr")[1].querySelector("th").getAttribute("class"),"slobodna", "Pada test!" );
                    k+=7;
                }
            }



        });

    });
});  